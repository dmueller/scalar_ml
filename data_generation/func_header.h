#define PHYSICAL_WEIGHTS true
#define FLUX_CONSERVATION_CHECK false
#define F_CHECK false
#define dim 2
#define nt 16
#define nx 16
int thermal_skip = 2000;
int f_max = 100;
double eta = 4.01;
double lam = 1.0;
extern int nsites;
extern int nlinks;
extern double mu;
extern double mu_step;
extern double mu_max;
