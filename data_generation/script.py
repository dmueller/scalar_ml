#!/usr/bin/env python3
# coding: utf-8

# In[ ]:

import argparse
import distutils.util
parser = argparse.ArgumentParser()
parser.add_argument('eta', type = float, help = "physical parameter equal to 2d+m^2")
parser.add_argument('lam', type = float, help = "value of the coupling constant")
parser.add_argument('mu', type = float, help = "value of the chemical potential")
parser.add_argument('dims', type = int, nargs = '+',
                    help = "lattice sites in each direction (dimensions from 2 to 4 are accepted in the format t x y z)")
parser.add_argument('--ndata', type = int, default = 4000, help = "number of measurements to be performed (4000 by default)")
parser.add_argument('--obs', type = lambda x: bool(distutils.util.strtobool(x)), default = True,
                    help = "set to false if you do not want to write observables on file")
parser.add_argument('--config', type = lambda x: bool(distutils.util.strtobool(x)), default = True,
                    help = "set to false if you do not want to write configurations on file")
parser.add_argument('--properties', type = lambda x: bool(distutils.util.strtobool(x)), default = False,
                    help = "set to true if you want to write properties of the worms on file")
parser.add_argument('--open_config', type = lambda x: bool(distutils.util.strtobool(x)), default = False,
                    help = "set to true if you want to write configurations with open worms on file")
parser.add_argument('--open_properties', type = lambda x: bool(distutils.util.strtobool(x)), default = False,
                    help = "set to true if you want to write properties of the open worms on file")
parser.add_argument('--open_worms', type = int, default = 1,
                    help = "number of open worms to generate (1 by default; if open_config and open_properties are false, no open worm will be created no matter its value)")
parser.add_argument('-t', '--thermal_skip', type = int, default = 1000,
                    help = "number of waiting iterations in the beginning, to skip non-thermalized data (1000 by default)")
parser.add_argument('-m', '--multimu', type = float, nargs = 2,
                    help = "repeats the simulation for different values of the chemical potential (first value = step, second value = maximum)")
parser.add_argument('-s','--sweeps', type = int, default = 5,
                    help = "number of l-sweeps and worms between each measurement (5 by default)")
parser.add_argument('-d', '--delay', type = int, default = 0,
                    help = "number of sweeps between measurements on closed and open worms (0 by default)")
parser.add_argument('-w', '--weights', choices = {'physical', 'unphysical'}, default = 'physical',
                    help = "specifies if physical (default) or unphysical weights are used")
parser.add_argument('--f_max', type = int, default = 100,
                    help = "expected maximum value of f(x) (100 by default)")
parser.add_argument('--flux_check', type = lambda x: bool(distutils.util.strtobool(x)), default = False,
                    help = "set to true to check if the k-flux is conserved after every worm is closed")
parser.add_argument('--f_check', type = lambda x: bool(distutils.util.strtobool(x)), default = False,
                    help = "set to true to check if f as defined in Gattringer's article is updated correctly")

args = parser.parse_args()

if args.eta <= 0:
    parser.error("eta must be positive")
if args.lam < 0:
    parser.error("lambda must be zero or positive")
if args.mu < 0:
    parser.error("the chemical potential must be zero or positive")
if len(args.dims) < 2 or len(args.dims) > 4:
    parser.error("dimensions can only be 2, 3 or 4")
for i in range(len(args.dims)):
    if args.dims[i] <= 0:
        parser.error("number of sites must be positive")
if args.ndata <= 0:
    parser.error("number of measurements must be positive")
if args.open_worms < 0:
    parser.error("number of open worms must be positive")
if args.open_worms == 0 and args.open_config and args.open_properties:
    parser.error("numer of open worms must be positive if you want to print configurations and/or properties of open worms")
if args.thermal_skip < 0:
    parser.error("iterations to skip must be zero or positive")
if args.multimu != None:
    if args.multimu[0] <= 0:
        parser.error("steps of the chemical potential must be positive")
    if args.multimu[1] < args.mu + args.multimu[0]:
        parser.error("the maximum chemical potential must be greater than the chemical potential plus the step")
if args.sweeps < 1:
    parser.error("there must be at least 1 sweep between each measurement")
if args.delay < 0 or args.delay >= args.sweeps:
    parser.error("delay must be in the range [0, sweeps)")
if args.f_max <= 0:
    parser.error("f_max must be positive")

eta = args.eta
lam = args.lam
dim = len(args.dims)
nt = args.dims[0]
nx = args.dims[1]
nsites = nt * nx
if dim > 2:
    ny = args.dims[2]
    nsites *= ny
    if dim > 3:
        nz = args.dims[3]
        nsites *= nz
nlinks = dim * nsites
ndata = args.ndata
thermal_skip = args.thermal_skip
mu = args.mu
if args.multimu != None:
    mustep = args.multimu[0]
    mumax = args.multimu[1] + mustep * .5
open_worms = args.open_worms
sweeps = args.sweeps
delay = args.delay
weights = args.weights
f_max = args.f_max

import numpy as np
import mpmath
from mpmath import mp
mp.prec = 100

def integrand(r, n, e, l):
    return mpmath.power(r, (n + 1)) * mpmath.exp(- e * r ** 2 - l * r ** 4)

Wratio = np.zeros(f_max // 2 + 1)

if weights == 'unphysical':
    W = np.zeros(f_max + 1)
    for n in range(f_max + 1):
        integral = mpmath.quad(lambda x: integrand(x, n, eta, lam), [0, mpmath.inf])
        W[n] = integral
else:
    W = np.zeros(f_max // 2 + 1)

for n in range(0, f_max + 2, 2):
    if weights == 'physical':
        integral = mpmath.quad(lambda x: integrand(x, n, eta, lam), [0, mpmath.inf])
        W[n // 2] = integral
    ratiointegral =  mpmath.quad(lambda x: integrand(x, n + 2, eta, lam), [0, mpmath.inf])/mpmath.quad(lambda x: integrand(x, n, eta, lam), [0, mpmath.inf])
    Wratio[n // 2] = ratiointegral

stringw = "double W[{}] = {{".format(len(W))
stringinvw = "double inv_W[{}] = {{".format(len(W))
stringwratio = "double W_ratio [{}] = {{".format(len(Wratio))
stringinvwratio = "double inv_W_ratio [{}] = {{".format(len(Wratio))

for i in range(len(W)):
    w = W[i]
    invw = 1/w
    stringw += str(w)
    stringinvw += str(invw)
    if i < len(W) - 1:
        stringw += ", "
        stringinvw += ", "
for i in range(len(Wratio)):
    wratio = Wratio[i]
    invwratio = 1/wratio
    stringwratio += str(wratio)
    stringinvwratio += str(invwratio)
    if i < len(Wratio) - 1:
        stringwratio += ", "
        stringinvwratio += ", "
stringw +=  "};\n"
stringinvw += "};\n"
stringwratio +=  "};\n"
stringinvwratio +=  "};\n"

file = open("W.h","w+")
file.write(stringw)
file.write(stringinvw)
file.write(stringwratio)
file.write(stringinvwratio)
file.close()

file2 = open("header.h","w+")
if args.obs :
    file2.write("#define OBS true\n")
else :
    file2.write("#define OBS false\n")
if args.config :
    file2.write("#define CONFIG true\n")
else :
    file2.write("#define CONFIG false\n")
if args.properties :
    file2.write("#define WORM_PROPERTIES true\n")
else :
    file2.write("#define WORM_PROPERTIES false\n")
if args.open_config :
    file2.write("#define OPEN_CONFIG true\n")
else :
    file2.write("#define OPEN_CONFIG false\n")
if args.open_properties :
    file2.write("#define OPEN_WORM_PROPERTIES true\n")
else :
    file2.write("#define OPEN_WORM_PROPERTIES false\n")
file2.write("#define delay {}\n".format(delay))
file2.write("int open_worms = {};\n".format(open_worms))
file2.write("int nsites = {};\n".format(nsites))
file2.write("int nlinks = {};\n".format(nlinks))
file2.write("int ndata = {};\n".format(ndata))
file2.write("double mu = {};\n".format(mu))
if args.multimu != None :
    file2.write("double mu_step = {};\n".format(mustep))
    file2.write("double mu_max = {};\n".format(mumax))
else :
    file2.write("double mu_step = {};\n".format(1.))
    file2.write("double mu_max = {};\n".format(mu))
file2.write("int sweeps = {};\n".format(sweeps))
file2.close()

file3 = open("func_header.h","w+")
if weights == "physical" :
    file3.write("#define PHYSICAL_WEIGHTS true\n")
else :
    file3.write("#define PHYSICAL_WEIGHTS false\n")
if args.flux_check :
    file3.write("#define FLUX_CONSERVATION_CHECK true\n")
else :
    file3.write("#define FLUX_CONSERVATION_CHECK false\n")
if args.f_check :
    file3.write("#define F_CHECK true\n")
else :
    file3.write("#define F_CHECK false\n")
file3.write("#define dim {}\n".format(dim))
file3.write("#define nt {}\n".format(nt))
file3.write("#define nx {}\n".format(nx))
if dim > 2 :
    file3.write("#define ny {}\n".format(ny))
    if dim > 3 :
        file3.write("#define nz {}\n".format(nz))   
file3.write("int thermal_skip = {};\n".format(thermal_skip))
file3.write("int f_max = {};\n".format(f_max))
file3.write("double eta = {};\n".format(eta))
file3.write("double lam = {};\n".format(lam))
file3.write("extern int nsites;\n")
file3.write("extern int nlinks;\n")
file3.write("extern double mu;\n")
file3.write("extern double mu_step;\n")
file3.write("extern double mu_max;\n")
file3.close()

print("Done writing weights and headers.")

import os
os.system("g++ -Ofast -o worm worm.cc functions.cc")
print("C++ program compiled.")
os.system("./worm")
print("Program executed.")
