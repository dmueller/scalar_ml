# This script tests the ensembles of models for each type.

python test_cmd.py "eq_arch" "equivariant" 1000 100 50
python test_cmd.py "st_arch" "nonequivariant_gp" 1000 100 50
python test_cmd.py "flat_arch" "nonequivariant_fl" 1000 100 50
